# N-body benchmark

## Description
An N-body simulation numerically approximates the evolution of a system of
bodies in which each body continuously interacts with every other body.  A
familiar example is an astrophysical simulation in which each body represents a
galaxy or an individual star, and the bodies attract each other through the
gravitational force.

N-body simulation arises in many other computational science problems as well.
For example, protein folding is studied using N-body simulation to calculate
electrostatic and van der Waals forces. Turbulent fluid flow simulation and
global illumination computation in computer graphics are other examples of
problems that use N-body simulation.

## Requirements
The requirements of this application are shown in the following lists. The main requirements are:

  * The **GNU** or **Intel** Compiler Collection.

  * A **Message Passing Interface (MPI)** implementation supporting the multi-threading
    level of thread support.

  * The **Task-Aware MPI (TAMPI)** library which defines a clean interoperability
    mechanism for MPI and OpenMP/OmpSs-2 tasks. It supports both blocking and non-blocking
    MPI operations by providing two different interoperability mechanisms. Downloads and
    more information at https://github.com/bsc-pm/tampi.

  * The **OmpSs-2** model which is the second generation of the OmpSs programming model.
    It is a task-based programming model originated from the ideas of the OpenMP and
    StarSs programming models. The specification and user-guide are available at
    https://pm.bsc.es/ompss-2-docs/spec/ and https://pm.bsc.es/ompss-2-docs/user-guide/,
    respectively. OmpSs-2 requires both Mercurium and Nanos6 tools. Mercurium is a
    source-to-source compiler which provides the necessary support for transforming the
    high-level directives into a parallelized version of the application. The Nanos6
    runtime system provides the services to manage all the parallelism in the application
    (e.g., task creation, synchronization, scheduling, etc.). Downloads at
    https://github.com/bsc-pm.

  * A derivative **Clang + LLVM OpenMP** that supports the non-blocking mode of TAMPI. Not
    released yet.

  * The **CUDA** tools and **NVIDIA Unified Memory** devices for enabling the CUDA variants,
    in which some of the N-body kernels are executed at the available GPU devices.

## Versions

The N-Body application has several versions which are built in different binaries. All of
them divide the particle space into smaller blocks. MPI processes are divided into two
groups: GPU processes and CPU processes. GPU processes are responsible for computing the
forces between each pair of particles blocks, and then, these forces are sent to the CPU
processes, where each process updates its particles blocks using the received forces. The
particles and forces blocks are equally distributed amongst each MPI process in each group.
Thus, each MPI process is in charge of computing the forces or updating the particles of a
consecutive chunk of blocks.

This computation pattern is repeated during multiple timesteps. The communication pattern
in each timestep consists in the fact that GPU processes exchange their particles with each
other in a circular manner to compute the forces between their particles against the
particles of all other GPU processes. For simplifying this pattern, the application uses a
different MPI communicator for this circular exchanging. Once a GPU process finishes the
computation of its forces, it sends the forces to the corresponding CPU processes, and then,
it receives the updated particles. We perform separate sends/receives for each block.

The available versions are:

  * `nbody.mpi.bin`: Simple MPI parallel version using blocking MPI primitives for sending
    and receiving each block of particles/forces.

  * `nbody.mpi.ompss2.bin`: Parallel version using MPI + OmpSs-2 tasks. Both computation
    and communication phases are taskified. However, communication tasks (each one sending
    or receiving a block) are serialized by an artificial dependency on a sentinel variable.
    This is to prevent deadlocks between processes since communication tasks perform blocking
    MPI calls.

  * `nbody.mpi.ompss2.cuda.bin`: The same as the previous version but offloading the tasks that
    compute the forces between particles blocks to the available GPUs. The GPU processes
    offload those computation tasks, which are the most compute-intensive parts of the program.
    The calculate_forces_block_cuda task is annotated as a regular task (e.g., with their
    dependencies) but implemented in CUDA. However, since it is Unified Memory, the user does
    not need to move the data to/from the GPU device.

  * `nbody.tampi.ompss2.bin`: Parallel version using MPI + OmpSs-2 tasks + TAMPI library. This
    version disables the artificial dependencies on the sentinel variable so that communication
    tasks can run in parallel and overlap with computations. The TAMPI library is in charge of
    managing the blocking MPI calls to avoid the blocking of the underlying execution resources.

  * `nbody.tampi.ompss2.cuda.bin`: A mix of the previous two variants where TAMPI is leveraged
    for allowing the concurrent execution of communication tasks, and GPU processes offload the
    compute-intensive tasks to the GPUs.

  * `nbody.mpi.omp.bin`: Parallel version using MPI + OpenMP tasks. Both computation and communication
    phases are taskified. However, communication tasks (each one sending or receiving a block) are
    serialized by an artificial dependency on a sentinel variable. This is to prevent deadlocks
    between processes since communication tasks perform blocking MPI calls.

  * `nbody.mpi.omptarget.bin`: The same as the previous version but offloading the tasks that
    compute the forces between particles blocks to the available GPUs. The GPU processes offload
    those computation tasks, which are the most compute-intensive parts of the program. This is
    done through the omp target directive, declaring the corresponding dependencies, and specifying
    the target as nowait (i.e., asynchronous offload). Additionally, the target directive does not
    require the user to provide a CUDA implementation of the offloaded task. Finally, since we are
    using the Unified Memory feature, we do not need to specify any data movement clause. We only
    have to specify that the memory buffers are already device pointers (i.e., with is_device_ptr
    clause). **Note:** This version is not compiled by default since it is still in a **Work in
    Progress** state.

  * `nbody.tampi.omp.bin`: Parallel version using MPI + OpenMP tasks + TAMPI library. This version
    disables the artificial dependencies on the sentinel variable so that communication tasks can
    run in parallel and overlap computations. Since OpenMP only supports the non-blocking mechanism
    of TAMPI, this version leverages non-blocking primitive calls. In this way, TAMPI library is in
    charge of managing the non-blocking MPI operations to overlap communication and computation tasks
    efficiently.

  * `nbody.tampi.omptarget.bin`: A mix of the previous two variants where TAMPI is leveraged for
    allowing the concurrent execution of communication tasks, and GPU processes offload the
    compute-intensive tasks to the GPUs. **Note:** This version is not compiled by default since it is
    still in a **Work in Progress** state.

The simplest way to compile this package is:

  1. Stay in N-Body root directory to recursively build all the versions. The
     TAMPI versions are compiled only if the `TAMPI_HOME` environment is set
     to the TAMPI library's installation directory. Similarly, CUDA versions
     are only compiled if `nvcc` compiler is available at the `$PATH`. In
     the DEEP system, you can load the required environment by executing
     `source setenv_deep.sh`.

  2. Type `make` to compile the selected benchmark's version(s). Optionally,
     you can indicate a different block size when building the benchmark (2048
     by default). Type `make BS=MY_BLOCK_SIZE` in order to change this value.

## Execution instructions

The binaries accept several options. The most relevant options are the number 
of total particles with `-p`, the number of timesteps with `-t`, and the maximum
number of GPU processes with `-g`. More options can be seen passing the `-h`
option. An example of execution could be:

```bash
$ srun -n 4 -c 3 ./nbody.tampi.ompss2.cuda.2048bs.bin -t 100 -p 8192 -g 2
```

in which the application will perform 100 timesteps in 4 MPI processes with 3 
cores per process (used by the OmpSs-2 runtime). The maximum number of GPU
processes is 2, so there will be 2 CPU processes and 2 GPU processes (assuming
all nodes have GPU devices). Since the total number of particles is 8192, each
process will be in charge of computing/updating 4096 forces/particles (2 blocks).

In the CUDA variants, a process can belong to the GPU processes group if it has
access to any GPU device. However, in the case of the non-CUDA versions, all
processes can belong to the GPU processes group. For this reason, the application
provides `-g` option in order to control the maximum number of GPU processes. By
default, the number of GPU processes will be half of the total number of
processes.

Also, note that the non-CUDA variants cannot compute kernels on the GPU. In this
cases, the structure of the application is kept but the CUDA tasks are replaced
by regular CPU tasks.
