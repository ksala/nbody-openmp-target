# Compilers
CLANG=clang
NVCC=nvcc
MPICC=mpicc
WRAPPERS=I_MPI_CC=$(CLANG) OMPI_CC=$(CLANG) MPICH_CC=$(CLANG)

# Use mpiicc if available
ifneq (, $(shell which mpiicc 2> /dev/null))
MPICC=mpiicc
endif

# Try to build the CUDA variant by default
CUDA?=1
TAMPI?=1

# Check for the CUDA compiler
ifeq (, $(shell which $(NVCC) 2> /dev/null))
CUDA=0
endif

# Block size
BS?=2048

# Preprocessor flags
CPPFLAGS=-Isrc -DBLOCK_SIZE=$(BS)
OMP_CPPFLAGS=-DUSE_OMP $(CPPFLAGS)
OMP_TARGET_CPPFLAGS=-DUSE_OMP_TARGET -DUSE_CUDA $(OMP_CPPFLAGS)
OMPSS2_CPPFLAGS=-DUSE_OMPSS2 $(CPPFLAGS)
OMPSS2_CUDA_CPPFLAGS=-DUSE_CUDA $(OMPSS2_CPPFLAGS)

# Compiler flags
CFLAGS=-O3 -std=gnu11
OMP_CFLAGS=-fopenmp $(CFLAGS)
OMP_TARGET_CFLAGS=-fopenmp -fopenmp-targets=nvptx64 $(CFLAGS)
OMPSS2_CFLAGS=--ompss-2 $(CFLAGS)
OMPSS2_CUDA_CFLAGS=--cuda $(OMPSS2_CFLAGS)

# Linker flags
LDFLAGS=-lrt -lm

# CUDA flags
CUDA_CPPFLAGS=-I$(CUDA_HOME)/include
CUDA_CFLAGS=-O3
CUDA_LDFLAGS=-L$(CUDA_HOME)/lib -L$(CUDA_HOME)/lib64 -lcudart

# Check for TAMPI_HOME environment variable
ifndef TAMPI_HOME
TAMPI=0
endif

# TAMPI flags
TAMPI_CPPFLAGS=-DUSE_TAMPI -I$(TAMPI_HOME)/include
TAMPI_LDFLAGS=-L$(TAMPI_HOME)/lib -L$(TAMPI_HOME)/lib64 -l:libtampi.a

COMMON_SOURCES=src/main.c src/misc.c

BINS+=nbody.mpi.omp.$(BS)bs.bin

ifeq ($(TAMPI),1)
BINS+=nbody.tampi.omp.$(BS)bs.bin
endif

ifeq ($(CUDA),1)
BINS+=nbody.mpi.omptarget.$(BS)bs.bin
endif

ifeq ($(TAMPI)$(CUDA),11)
BINS+=nbody.tampi.omptarget.$(BS)bs.bin
endif

all: $(BINS)

nbody.mpi.omp.$(BS)bs.bin: $(COMMON_SOURCES) src/solver.mpi.omp.c
	$(WRAPPERS) $(MPICC) $(OMP_CPPFLAGS) $(OMP_CFLAGS) -o $@ $^ $(LDFLAGS)

nbody.tampi.omp.$(BS)bs.bin: $(COMMON_SOURCES) src/solver.mpi.omp.c
	$(WRAPPERS) $(MPICC) $(OMP_CPPFLAGS) $(TAMPI_CPPFLAGS) $(OMP_CFLAGS) -o $@ $^ $(LDFLAGS) $(TAMPI_LDFLAGS)

nbody.mpi.omptarget.$(BS)bs.bin: $(COMMON_SOURCES) src/solver.mpi.omp.c
	$(WRAPPERS) $(MPICC) $(OMP_TARGET_CPPFLAGS) $(CUDA_CPPFLAGS) $(OMP_TARGET_CFLAGS) -o $@ $^ $(LDFLAGS) $(CUDA_LDFLAGS) -L/usr/lib/gcc/ppc64le-redhat-linux/4.8.5/

nbody.tampi.omptarget.$(BS)bs.bin: $(COMMON_SOURCES) src/solver.mpi.omp.c
	$(WRAPPERS) $(MPICC) $(OMP_TARGET_CPPFLAGS) $(CUDA_CPPFLAGS) $(TAMPI_CPPFLAGS) $(OMP_TARGET_CFLAGS) -o $@ $^ $(LDFLAGS) $(CUDA_LDFLAGS) $(TAMPI_LDFLAGS)

clean:
	rm -f *.o *.bin
