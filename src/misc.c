#define _XOPEN_SOURCE 500

#include "nbody.h"

#include <mpi.h>

#include <assert.h>
#include <ctype.h>
#include <fcntl.h>
#include <getopt.h>
#include <ieee754.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#ifdef USE_OMPSS2
#include <nanos6/debug.h>
#endif

#ifdef USE_OMP
#include <omp.h>
#endif

#ifdef USE_CUDA
#include <cuda_runtime.h>
#endif

static void *allocate(size_t size)
{
	void *pointer = NULL;
#ifdef USE_CUDA
	cudaError_t err = cudaMallocManaged(&pointer, size, cudaMemAttachGlobal);
	assert(err == cudaSuccess);
#else
	pointer = malloc(size);
#endif
	assert(pointer != NULL);

	return pointer;
}

static void deallocate(void *pointer)
{
#ifdef USE_CUDA
	cudaError_t err = cudaFree(pointer);
	assert(err == cudaSuccess);
#else
	free(pointer);
#endif
}

void nbody_generate_particles(const nbody_conf_t *conf, const nbody_file_t *file)
{
	char fname[1024];
	sprintf(fname, "%s.in", file->name);

	if (!access(fname, F_OK)) {
		return;
	}

	struct stat st = {0};
	if (stat("data", &st) == -1) {
		mkdir("data", 0755);
	}

	const int fd = open(fname, O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IRGRP|S_IROTH);
	if (fd < 0) {
		perror(NULL);
	}
	assert(fd >= 0);

	const int total_size = file->total_size;

	int err = ftruncate(fd, total_size);
	assert(!err);

	particles_block_t * const particles = mmap(NULL, total_size, PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);

	for (int b = 0; b < conf->num_blocks; b++) {
		nbody_particle_init(conf, particles+b);
	}

	err = munmap(particles, total_size);
	assert(!err);

	err = close(fd);
	assert(!err);
}

void nbody_check(const nbody_t *nbody)
{
	assert(nbody != NULL);

	char fname[1024];
	sprintf(fname, "%s.ref", nbody->file.name);
	if (access(fname, F_OK) != 0) {
		if (!rank) fprintf(stderr, "Warning: %s file does not exist. Skipping the check...\n", fname);
		return;
	}

	const int fd = open(fname, O_RDONLY, 0);
	assert(fd >= 0);

	particles_block_t *reference = mmap(NULL, nbody->file.size, PROT_READ, MAP_SHARED, fd, nbody->file.offset);
	assert(reference != MAP_FAILED);

	int correct, correct_chunk;
	correct_chunk = nbody_compare_particles(nbody->local, reference, nbody->num_blocks);

	MPI_Reduce(&correct_chunk, &correct, 1, MPI_INT, MPI_LAND, 0, MPI_COMM_WORLD);

	if (!rank) {
		if (correct) {
			printf("Result validation: OK\n");
		} else {
			printf("Result validation: ERROR\n");
		}
	}

	int err = munmap(reference, nbody->file.size);
	assert(!err);

	err = close(fd);
	assert(!err);
}

void nbody_setup_file(const nbody_conf_t *conf, nbody_file_t *file)
{
	assert(conf != NULL);
	assert(file != NULL);

	file->total_size = conf->num_blocks * sizeof(particles_block_t);
	file->size = file->total_size / num_ranks;
	file->offset = rank * file->size;

	sprintf(file->name, "%s-N2-%d-%d-%d", conf->name, conf->num_particles, BLOCK_SIZE, conf->timesteps);
}

particles_block_t *nbody_load_particles(const nbody_file_t *file)
{
	char fname[1024];
	sprintf(fname, "%s.in", file->name);

	struct stat st;
	stat(fname, &st);
	assert(st.st_size == file->total_size);

	const int fd = open(fname, O_RDONLY, 0);
	assert(fd >= 0);

	void * const file_particles = mmap(NULL, file->size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, file->offset);
	assert(file_particles != MAP_FAILED);

	void *particles = allocate(file->size);
	assert(particles != NULL);

	memcpy(particles, file_particles, file->size);

	int err = close(fd);
	assert(!err);

	err = munmap(file_particles, file->size);
	assert(!err);

	return particles;
}

void nbody_setup(const nbody_conf_t *conf, nbody_t *nbody)
{
	assert(conf != NULL);
	assert(nbody != NULL);

	nbody->num_blocks = conf->num_blocks / num_ranks;
	nbody->num_particles = nbody->num_blocks * BLOCK_SIZE;
	nbody->timesteps = conf->timesteps;

	const size_t particles_size = nbody->num_blocks * sizeof(particles_block_t);
	const size_t forces_size = nbody->num_blocks * sizeof(forces_block_t);

	nbody_setup_file(conf, &nbody->file);

	if (rank == 0)
		nbody_generate_particles(conf, &nbody->file);

	MPI_Barrier(MPI_COMM_WORLD);

	nbody->local = nbody_load_particles(&nbody->file);
	nbody->remote1 = (particles_block_t *) allocate(particles_size);
	nbody->remote2 = (particles_block_t *) allocate(particles_size);
	nbody->forces = (forces_block_t *) allocate(forces_size);

	assert(nbody->local != NULL);
	assert(nbody->remote1 != NULL);
	assert(nbody->remote2 != NULL);
	assert(nbody->forces != NULL);
}

void nbody_free(nbody_t *nbody)
{
	deallocate(nbody->local);
	deallocate(nbody->remote1);
	deallocate(nbody->remote2);
	deallocate(nbody->forces);
}

void nbody_save_particles(const nbody_t *nbody)
{
	assert(nbody != NULL);

	char fname[1024];
	sprintf(fname, "%s.out", nbody->file.name);
	const int mode = MPI_MODE_CREATE | MPI_MODE_WRONLY;

	MPI_File outfile;
	int err = MPI_File_open(MPI_COMM_WORLD, fname, mode, MPI_INFO_NULL, &outfile);
	assert(err == MPI_SUCCESS);

	err = MPI_File_set_view(outfile, nbody->file.offset, MPI_BYTE, MPI_BYTE, "native", MPI_INFO_NULL);
	assert(err == MPI_SUCCESS);

	err = MPI_File_write(outfile, nbody->local, nbody->file.size, MPI_BYTE, MPI_STATUS_IGNORE);
	assert(err == MPI_SUCCESS);

	err = MPI_File_close(&outfile);
	assert(err == MPI_SUCCESS);

	MPI_Barrier(MPI_COMM_WORLD);
}

static void nbody_print_usage(int argc, char **argv)
{
	fprintf(stderr, "Usage: %s <-p particles> <-t timesteps> [OPTION]...\n", argv[0]);
	fprintf(stderr, "Parameters:\n");
	fprintf(stderr, "  -p, --particles=PARTICLES  the total number of particles (default: 16384)\n");
	fprintf(stderr, "  -t, --timesteps=TIMESTEPS  the number of timesteps (default: 10)\n\n");
	fprintf(stderr, "Optional parameters:\n");
	fprintf(stderr, "  -c, --check                check the correctness of the result (default: disabled)\n");
	fprintf(stderr, "  -o, --output               save the computed particles to the default output file (default: disabled)\n");
	fprintf(stderr, "  -h, --help                 display this help and exit\n\n");
}

void nbody_get_conf(int argc, char **argv, nbody_conf_t *conf)
{
	assert(conf != NULL);

	if (rank == 0) {
		conf->domain_size_x = default_domain_size_x;
		conf->domain_size_y = default_domain_size_y;
		conf->domain_size_z = default_domain_size_z;
		conf->mass_maximum  = default_mass_maximum;
		conf->time_interval = default_time_interval;
		conf->seed          = default_seed;
		conf->num_particles = default_num_particles;
		conf->num_blocks    = conf->num_particles / BLOCK_SIZE;
		conf->timesteps     = default_timesteps;
		conf->save_result   = default_save_result;
		conf->check_result  = default_check_result;
		strcpy(conf->name, default_name);

		static struct option long_options[] = {
			{"particles",     required_argument, 0, 'p'},
			{"timesteps",     required_argument, 0, 't'},
			{"check",         no_argument,       0, 'c'},
			{"output",        no_argument,       0, 'o'},
			{"help",          no_argument,       0, 'h'},
			{0, 0, 0, 0}
		};

		int c;
		int index;
		while ((c = getopt_long(argc, argv, "hocp:t:", long_options, &index)) != -1) {
			switch (c) {
				case 'h':
					nbody_print_usage(argc, argv);
					exit(0);
				case 'o':
					conf->save_result = 1;
					break;
				case 'c':
					conf->check_result = 1;
					break;
				case 'p':
					conf->num_particles = atoi(optarg);
					break;
				case 't':
					conf->timesteps = atoi(optarg);
					break;
				case '?':
					exit(1);
				default:
					abort();
			}
		}

		if (conf->num_particles < 1 || conf->timesteps < 1) {
			nbody_print_usage(argc, argv);
			exit(1);
		}

		conf->num_particles = ROUNDUP(conf->num_particles, MIN_PARTICLES);
		assert(conf->num_particles >= BLOCK_SIZE);

		conf->num_blocks = conf->num_particles / BLOCK_SIZE;
		assert(conf->num_blocks > 0);
	}

	MPI_Bcast(conf, sizeof(nbody_conf_t), MPI_BYTE, 0, MPI_COMM_WORLD);
	assert(conf->num_particles > 0);
	assert(conf->timesteps > 0);
}

void nbody_particle_init(const nbody_conf_t *conf, particles_block_t *particles)
{
	for (int i = 0; i < BLOCK_SIZE; i++){
		particles->position_x[i] = conf->domain_size_x * ((float)random() / ((float)RAND_MAX + 1.0));
		particles->position_y[i] = conf->domain_size_y * ((float)random() / ((float)RAND_MAX + 1.0));
		particles->position_z[i] = conf->domain_size_z * ((float)random() / ((float)RAND_MAX + 1.0));
		particles->mass[i] = conf->mass_maximum * ((float)random() / ((float)RAND_MAX + 1.0));
		particles->weight[i] = gravitational_constant * particles->mass[i];
	}
}

int nbody_compare_particles(const particles_block_t *local, const particles_block_t *reference, int num_blocks)
{
	double error = 0.0;
	int count = 0;
	for (int i = 0; i < num_blocks; i++) {
		for (int e = 0; e < BLOCK_SIZE; e++) {
			if ((local[i].position_x[e] != reference[i].position_x[e]) ||
			    (local[i].position_y[e] != reference[i].position_y[e]) ||
			    (local[i].position_z[e] != reference[i].position_z[e])) {
					error += fabs(((local[i].position_x[e] - reference[i].position_x[e])*100.0) / reference[i].position_x[e]) +
					         fabs(((local[i].position_y[e] - reference[i].position_y[e])*100.0) / reference[i].position_y[e]) +
					         fabs(((local[i].position_z[e] - reference[i].position_z[e])*100.0) / reference[i].position_z[e]);
					count++;
			}
		}
	}

	double relative_error = (count != 0) ? error / (3.0 * count) : 0.0;
	if ((count * 100.0) / (num_blocks * BLOCK_SIZE) > 0.6 || relative_error > TOLERATED_ERROR) {
		return 0;
	}
	return 1;
}

double nbody_compute_throughput(int num_particles, int timesteps, double elapsed_time)
{
	double interactions_per_timestep = (double)(num_particles)*(double)(num_particles);
	return (((interactions_per_timestep * (double)timesteps) / elapsed_time) / 1000000.0);
}

void nbody_stats(const nbody_t *nbody, const nbody_conf_t *conf, double time)
{
	if (!rank) {
		int num_cpus = 1;
#if defined(USE_OMPSS2)
		num_cpus = nanos6_get_num_cpus();
#elif defined(USE_OMP)
		num_cpus = omp_get_max_threads();
#endif
		double throughput = nbody_compute_throughput(conf->num_particles, nbody->timesteps, time);

		printf("bigo, N2, ranks, %d, threads, %d, timesteps, %d, total_particles, %d, block_size, %d, time, %.2f, throughput, %.2f\n",
			num_ranks, num_cpus, nbody->timesteps, conf->num_particles, BLOCK_SIZE, time, throughput
		);
	}
}

double get_time()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (double)(ts.tv_sec) + (double)ts.tv_nsec * 1.0e-9;
}
