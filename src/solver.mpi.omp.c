#include "nbody.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <mpi.h>

#ifdef USE_TAMPI
#include <TAMPI.h>
#endif

static void calculate_forces(forces_block_t *forces, const particles_block_t *block1, const particles_block_t *block2, int num_blocks);
static void exchange_particles(const particles_block_t *sendbuf, particles_block_t *recvbuf, int num_blocks, int tagbase);
static void update_particles(particles_block_t *particles, forces_block_t *forces, int num_blocks, float time_interval);
static void send_particles(const particles_block_t *particles, int num_blocks, int *dsts);
static void recv_particles(particles_block_t *particles, int num_blocks, int *srcs);
static void send_forces(const forces_block_t *forces, int num_blocks, int *dsts);
static void recv_forces(forces_block_t *forces, int num_blocks, int *srcs);


static inline int get_shift_tagbase(int iteration, int num_blocks)
{
	// We define three different MPI tagbases depending on the
	// shift iteration
	// - First iteration: [num_blocks*2, num_blocks*3)
	// - Odd iterations:  [num_blocks, num_blocks*2)
	// - Even iterations: [0, num_blocks)
	if (iteration == 0) {
		return num_blocks * 2;
	} else if (iteration % 2) {
		return num_blocks;
	} else {
		return 0;
	}
}

void nbody_solve(nbody_t *nbody, int num_blocks, int timesteps, float time_interval)
{
	assert(nbody != NULL);
	assert(timesteps > 0);

	particles_block_t *local = nbody->local;
	particles_block_t *remote1 = nbody->remote1;
	particles_block_t *remote2 = nbody->remote2;
	forces_block_t *forces = nbody->forces;

	#pragma omp parallel
	#pragma omp single
	for (int t = 0; t < timesteps; t++) {
		particles_block_t *sendbuf = local;
		particles_block_t *recvbuf = remote1;

		for (int r = 0; r < num_ranks; r++) {
			// Compute the forces between particles
			calculate_forces(forces, local, sendbuf, num_blocks);

			if (r < num_ranks - 1) {
				// Since we leverage a double buffering technique, we
				// need to define different MPI tag domains for each
				// communication buffer. Otherwise, we would use the
				// same tags from concurrent communication tasks. In
				// fact, this is only needed when enabling TAMPI (no
				// serialization of communication tasks).
				const int tagbase = get_shift_tagbase(r, num_blocks);

				// Send the current particles and receive the next one
				exchange_particles(sendbuf, recvbuf, num_blocks, tagbase);
			}

			// Swap buffers
			particles_block_t *aux = recvbuf;
			recvbuf = (r != 0) ? sendbuf : remote2;
			sendbuf = aux;
		}

		// Update the particles in the CPU
		update_particles(local, forces, num_blocks, time_interval);
	}

	MPI_Barrier(MPI_COMM_WORLD);
}

#ifdef USE_OMP_TARGET
#pragma omp declare target
#endif
static void calculate_forces_block(forces_block_t *forces, const particles_block_t *block1, const particles_block_t *block2)
{
	float *x = forces->x;
	float *y = forces->y;
	float *z = forces->z;

	const int same_block = (block1 == block2);
	const float *pos_x1 = block1->position_x;
	const float *pos_y1 = block1->position_y;
	const float *pos_z1 = block1->position_z;
	const float *mass1  = block1->mass;

	const float *pos_x2 = block2->position_x;
	const float *pos_y2 = block2->position_y;
	const float *pos_z2 = block2->position_z;
	const float *mass2  = block2->mass;

	const float gravitational_constant_ = 6.6726e-11f;

#ifdef USE_OMP_TARGET
	//#pragma omp parallel for
	//#pragma omp teams distribute parallel for collapse(2) num_teams(1024) thread_limit(256)
	#pragma omp distribute parallel for
#endif
	for (int i = 0; i < BLOCK_SIZE; i++) {
		float fx = x[i], fy = y[i], fz = z[i];
		for (int j = 0; j < BLOCK_SIZE; j++) { // NOTE: Cannot be collapsed directly
			const float diff_x = pos_x2[j] - pos_x1[i];
			const float diff_y = pos_y2[j] - pos_y1[i];
			const float diff_z = pos_z2[j] - pos_z1[i];

			const float distance_squared = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
			const float distance = sqrtf(distance_squared);

			float force = 0.0f;
			if (!same_block || distance_squared != 0.0f) {
				force = (mass1[i] / (distance_squared * distance)) * (mass2[j] * gravitational_constant_);
			}
			fx += force * diff_x;
			fy += force * diff_y;
			fz += force * diff_z;
		}
		x[i] = fx;
		y[i] = fy;
		z[i] = fz;
	}
}
#ifdef USE_OMP_TARGET
#pragma omp end declare target
#endif

static void update_particles_block(particles_block_t *particles, forces_block_t *forces, float time_interval)
{
	for (int e = 0; e < BLOCK_SIZE; e++){
		const float mass       = particles->mass[e];
		const float velocity_x = particles->velocity_x[e];
		const float velocity_y = particles->velocity_y[e];
		const float velocity_z = particles->velocity_z[e];
		const float position_x = particles->position_x[e];
		const float position_y = particles->position_y[e];
		const float position_z = particles->position_z[e];

		const float time_by_mass       = time_interval / mass;
		const float half_time_interval = 0.5f * time_interval;

		const float velocity_change_x = forces->x[e] * time_by_mass;
		const float velocity_change_y = forces->y[e] * time_by_mass;
		const float velocity_change_z = forces->z[e] * time_by_mass;
		const float position_change_x = velocity_x + velocity_change_x * half_time_interval;
		const float position_change_y = velocity_y + velocity_change_y * half_time_interval;
		const float position_change_z = velocity_z + velocity_change_z * half_time_interval;

		particles->velocity_x[e] = velocity_x + velocity_change_x;
		particles->velocity_y[e] = velocity_y + velocity_change_y;
		particles->velocity_z[e] = velocity_z + velocity_change_z;
		particles->position_x[e] = position_x + position_change_x;
		particles->position_y[e] = position_y + position_change_y;
		particles->position_z[e] = position_z + position_change_z;
	}

	memset(forces, 0, sizeof(forces_block_t));
}

#ifdef USE_TAMPI
static void send_particles_block(const particles_block_t *block, int tag, int dst)
{
	#pragma omp task depend(in: block[0])
	{
		MPI_Request req;
		MPI_Isend(block, sizeof(particles_block_t), MPI_BYTE, dst, tag, MPI_COMM_WORLD, &req);
		TAMPI_Iwait(&req, MPI_STATUS_IGNORE);
	}
}

static void recv_particles_block(particles_block_t *block, int tag, int src)
{
	#pragma omp task depend(out: block[0])
	{
		MPI_Request req;
		MPI_Irecv(block, sizeof(particles_block_t), MPI_BYTE, src, tag, MPI_COMM_WORLD, &req);
		TAMPI_Iwait(&req, MPI_STATUS_IGNORE);
	}
}
#else
// Use a sentinel variable to serialize all communication tasks
int serial;

static void send_particles_block(const particles_block_t *block, int tag, int dst)
{
	#pragma omp task depend(in: block[0]) depend(inout: serial)
	MPI_Send(block, sizeof(particles_block_t), MPI_BYTE, dst, tag, MPI_COMM_WORLD);
}

static void recv_particles_block(particles_block_t *block, int tag, int src)
{
	#pragma omp task depend(out: block[0]) depend(inout: serial)
	MPI_Recv(block, sizeof(particles_block_t), MPI_BYTE, src, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}
#endif

void calculate_forces(forces_block_t *forces, const particles_block_t *block1, const particles_block_t *block2, int num_blocks)
{
	for (int i = 0; i < num_blocks; i++) {
		for (int j = 0; j < num_blocks; j++) {
#ifdef USE_OMP_TARGET
			#pragma omp target depend(in: block1[i], block2[j]) depend(inout: forces[i]) is_device_ptr(block1, block2, forces) nowait
			#pragma omp teams num_teams(BLOCK_SIZE/256) thread_limit(256)
			calculate_forces_block(forces+i, block1+i, block2+j);
#else
			#pragma omp task depend(in: block1[i], block2[j]) depend(inout: forces[i])
			calculate_forces_block(forces+i, block1+i, block2+j);
#endif
		}
	}
}

void update_particles(particles_block_t *particles, forces_block_t *forces, int num_blocks, float time_interval)
{
	for (int b = 0; b < num_blocks; ++b) {
		#pragma omp task depend(inout: particles[b]) depend(inout: forces[b])
		update_particles_block(particles+b, forces+b, time_interval);
	}
}

void exchange_particles(const particles_block_t *sendbuf, particles_block_t *recvbuf, int num_blocks, int tagbase)
{
	int src = MOD(rank - 1, num_ranks);
	int dst = MOD(rank + 1, num_ranks);

	if (rank % 2) {
		for (int b = 0; b < num_blocks; ++b) {
			send_particles_block(sendbuf+b, tagbase+b, dst);
			recv_particles_block(recvbuf+b, tagbase+b, src);
		}
	} else {
		for (int b = 0; b < num_blocks; ++b) {
			recv_particles_block(recvbuf+b, tagbase+b, src);
			send_particles_block(sendbuf+b, tagbase+b, dst);
		}
	}
}
