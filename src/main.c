#include "nbody.h"

#include <mpi.h>

#ifdef USE_TAMPI
#include <TAMPI.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if defined(USE_OMPSS2) && defined(USE_TAMPI)
static const int REQUESTED_LEVEL = MPI_TASK_MULTIPLE;
#else
static const int REQUESTED_LEVEL = MPI_THREAD_MULTIPLE;
#endif

int rank, num_ranks;

int main(int argc, char** argv)
{
	int provided;
	MPI_Init_thread(&argc, &argv, REQUESTED_LEVEL, &provided);
	if (provided != REQUESTED_LEVEL) {
		fprintf(stderr, "The requested threading level is not supported");
		return 1;
	}

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

	nbody_conf_t conf;
	nbody_get_conf(argc, argv, &conf);

	nbody_t nbody;
	nbody_setup(&conf, &nbody);
	MPI_Barrier(MPI_COMM_WORLD);

	double start = get_time();
	nbody_solve(&nbody, nbody.num_blocks, nbody.timesteps, conf.time_interval);
	double end = get_time();

	nbody_stats(&nbody, &conf, end - start);

	if (conf.save_result) nbody_save_particles(&nbody);
	if (conf.check_result) nbody_check(&nbody);
	nbody_free(&nbody);

	MPI_Finalize();
	return 0;
}
